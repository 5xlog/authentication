package com.pentastagiu.security;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.pentastagiu.models.Account;
import com.pentastagiu.repository.AccountRepository;

public class CustomUserDetailsServiceTest {

	private final String TEST_USERNAME = "bogdan"; 
	
	@Mock
	private AccountRepository accountRepository;

	@InjectMocks
	private CustomUserDetailsService userDetailsService;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void loadUserByUsernameShouldReturnAccountPrincipal() {
		Account user = new Account();
		user.setUsername(TEST_USERNAME);
		Optional<Account> account = Optional.of(user);
		AccountPrincipal accountPrincipal = new AccountPrincipal(account.get());
		when(accountRepository.findOneByUsernameOrEmail(Mockito.any(), Mockito.any())).thenReturn(account);

		assertEquals(accountPrincipal.getUsername(), userDetailsService.loadUserByUsername(TEST_USERNAME).getUsername());
	}

	@Test(expected = UsernameNotFoundException.class)
	public void loadUserByUsernameShouldThrowException() {
		when(accountRepository.findOneByUsernameOrEmail(Mockito.any(), Mockito.any())).thenReturn(Optional.empty());

		userDetailsService.loadUserByUsername("");

	}

}
