package com.pentastagiu.service;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import com.pentastagiu.dto.AccountDto;
import com.pentastagiu.utils.AccountValidationConstants;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class RegisterServiceTest {

	private static final Long ACCOUND_ID_TEST = (long) 1;

	@Autowired
	private RegisterService registerService;

	@MockBean
	private AccountCreation accountCreation;

	@Test
	public void performRegisterShouldReturnEmptyList() {
		when(accountCreation.createCartUponAccountCreation(Mockito.anyLong())).thenReturn(HttpStatus.OK);

		AccountDto accountDto = new AccountDto("Username", "Password123@", "Lastname", "Firstname", "emailp@ceva.com",
				"0712345678");

		List<String> registerServiceResponse = registerService.register(accountDto);

		assertThat(registerServiceResponse.isEmpty(), is(true));
	}

	@Test
	public void performRegisterShouldReturnUsernameIsWrong() {
		AccountDto accountDto = new AccountDto("123", "Password123@", "Lastname", "Firstname", "emailp@ceva.com",
				"0712345678");

		List<String> registerServiceResponse = registerService.register(accountDto);

		assertThat(registerServiceResponse.isEmpty(), is(false));
		assertEquals(registerServiceResponse.get(0), AccountValidationConstants.USERNAME_ERROR_MESSAGE);
	}

	@Test
	public void performRegisterShouldReturnEmailIsWrong() {
		AccountDto accountDto = new AccountDto("Username", "Password123@", "Lastname", "Firstname", "email",
				"0712345678");

		List<String> registerServiceResponse = registerService.register(accountDto);

		assertThat(registerServiceResponse.isEmpty(), is(false));
		assertEquals(registerServiceResponse.get(0), AccountValidationConstants.EMAIL_ERROR_MESSAGE);
	}

	@Test
	public void shouldDeleteAccount() {
		when(accountCreation.createCartUponAccountCreation(ACCOUND_ID_TEST)).thenReturn(HttpStatus.BAD_REQUEST);

		AccountDto accountDto = new AccountDto("Username", "Password123@", "Lastname", "Firstname", "emailp@ceva.com",
				"0712345678");

		List<String> registerServiceResponse = registerService.register(accountDto);

		assertThat(registerServiceResponse.isEmpty(), is(false));
		assertEquals(registerServiceResponse.get(0), AccountValidationConstants.REGISTER_ERROR_MESSAGE);
	}

}
