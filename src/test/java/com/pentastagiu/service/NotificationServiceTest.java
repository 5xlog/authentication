package com.pentastagiu.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.client.utils.URIBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
public class NotificationServiceTest {
	@Mock
	RestTemplate restTemplate;

	@InjectMocks
	NotificationService notificationService;

	static String uri = "http://localhost:8094/notification";

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testSendNotificationShouldWorkFine() throws URISyntaxException {

		URIBuilder uriBuilder;
		uriBuilder = new URIBuilder("http://localhost:8094/notification");
		ReflectionTestUtils.setField(notificationService, "uri", "http://localhost:8094/notification");
		uriBuilder.addParameter("email", "email");
		uriBuilder.addParameter("last_name", "lastName");
		uriBuilder.addParameter("first_name", "firstName");
		uriBuilder.addParameter("type", "type");

		URI uriString = uriBuilder.build();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<String> entity = new HttpEntity<String>(null, headers);
		notificationService.sendNotification("email", "lastName", "firstName", "type");

		verify(restTemplate, times(1)).exchange(uriString, HttpMethod.POST, entity, String.class);
	}
}
