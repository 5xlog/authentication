package com.pentastagiu.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.pentastagiu.dto.ProductDTO;
import com.pentastagiu.response.Response;

@RunWith(SpringRunner.class)
public class ProductServiceTest {

	@Mock
	RestTemplate restTemplate;

	@InjectMocks
	ProductService productService;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@SuppressWarnings("rawtypes")
	@Test
	public void testEditProductShouldReturnAccepted() {
		ReflectionTestUtils.setField(productService, "uri", "http://localhost:8091/product/{id}");

		ProductDTO productDTO = new ProductDTO();
		HttpEntity<ProductDTO> request = new HttpEntity<ProductDTO>(productDTO);
		ResponseEntity<Response> response = new ResponseEntity<Response>(HttpStatus.ACCEPTED);
		long id = 1L;
		when(restTemplate.exchange("http://localhost:8091/product/{id}", HttpMethod.PUT, request, Response.class, id))
				.thenReturn(response);
		ResponseEntity<Response> editResponse = productService.editProduct(id, productDTO);
		assertEquals(response.getStatusCode().toString(), editResponse.getStatusCode().toString());
	}

	@SuppressWarnings("rawtypes")
	@Test
	public void testEditProductShouldReturnNotFound() {
		ReflectionTestUtils.setField(productService, "uri", "http://localhost:8091/product/{id}");
		ProductDTO productDTO = new ProductDTO();
		HttpEntity<ProductDTO> request = new HttpEntity<ProductDTO>(productDTO);
		ResponseEntity<Response> response = new ResponseEntity<Response>(HttpStatus.NOT_FOUND);
		HttpClientErrorException exception = HttpClientErrorException.create(HttpStatus.NOT_FOUND, "status", null, null,
				null);
		when(restTemplate.exchange("http://localhost:8091/product/{id}", HttpMethod.PUT, request, Response.class, 1L))
				.thenThrow(exception);
		long id = 1L;
		ResponseEntity<Response> editResponse = productService.editProduct(id, productDTO);

		assertEquals(response.getStatusCode().toString(), editResponse.getStatusCode().toString());
	}
}
