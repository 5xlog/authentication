package com.pentastagiu.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestClientException;

import com.pentastagiu.enums.Role;
import com.pentastagiu.enums.Status;
import com.pentastagiu.models.Account;
import com.pentastagiu.repository.AccountRepository;

@RunWith(SpringRunner.class)
public class AccountServiceTest {

	@Mock
	AccountRepository accountRepository;

	@Mock
	NotificationService notificationService;

	@InjectMocks
	AccountService accountService;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(accountService);
	}

	@Test
	public void testChangePasswordShouldReturnOkStatus() {
		Account account = new Account("usernamaaae12", "", Role.USER, "asd", "wewe", "ceva@ceva.com", "0712345678");
		when(accountRepository.findOneByUsername("usernamaaae12")).thenReturn(Optional.of(account));

		assertEquals(Status.OK.toString(), accountService.changePassword("usernamaaae12", "Parola123@")
				.getResponseStatuses().get(0).getStatus().toString());
	}

	@Test(expected = SecurityException.class)
	public void testChangePasswordShouldReturnBadRequestForBadPassword() {
		Account account = new Account("usernamaaae12", "Parola123", Role.USER, "asd", "wewe", "ceva@ceva.com",
				"0712345678");
		when(accountRepository.findOneByUsername(Mockito.any())).thenReturn(Optional.of(account));

		assertEquals(Status.ERROR.toString(),
				accountService.changePassword("usernamaaae12", "").getResponseStatuses().get(0).getStatus().toString());
	}

	@Test(expected = RestClientException.class)
	public void testChangePasswordShouldThrowException() {
		Account account = new Account("usernamaaae12", "Parola123@", Role.USER, "asd", "wewe", "ceva@ceva.com",
				"0712345678");
		doThrow(RestClientException.class).when(notificationService).sendNotification(Mockito.any(), Mockito.any(),
				Mockito.any(), Mockito.any());
		when(accountRepository.findOneByUsername(Mockito.any())).thenReturn(Optional.of(account));
		assertEquals(Status.ERROR.toString(), accountService.changePassword("usernamaaae12", "Parola123@")
				.getResponseStatuses().get(0).getStatus().toString());
	}
}
