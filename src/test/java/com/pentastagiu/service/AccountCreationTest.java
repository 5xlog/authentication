package com.pentastagiu.service;

import static org.junit.Assert.assertEquals;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.client.utils.URIBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import com.pentastagiu.enums.NotificationServiceParameters;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AccountCreationTest {

	private static final Long ACCOUND_ID_TEST = (long) 1;
	private static final String EMAIL_FOR_TEST = "email";
	private static final String LAST_NAME_FOR_TEST = "lastname";
	private static final String FIRST_NAME_FOR_TEST = "firstname";

	private static final String CART_SERVICE_URL = "http://localhost:8092/v1/cart?accountId=";
	private static final String NOTIFICATION_SERVICE_URL = "http://localhost:8094/notification";

	@Mock
	RestTemplate restTemplate;

	@InjectMocks
	AccountCreation accountCreation;

	@Before
	public void setUp() throws Exception {

		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void createCartShouldReturnStatusOk() throws URISyntaxException {

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<String> entity = new HttpEntity<String>(null, headers);

		Mockito.when(restTemplate.exchange(CART_SERVICE_URL + ACCOUND_ID_TEST, HttpMethod.POST, entity, String.class))
				.thenReturn(new ResponseEntity<String>(HttpStatus.ACCEPTED));

		ReflectionTestUtils.setField(accountCreation, "cartServiceUrl", CART_SERVICE_URL);

		HttpStatus result = accountCreation.createCartUponAccountCreation(ACCOUND_ID_TEST);

		assertEquals(HttpStatus.ACCEPTED, result);
	}

	@Test
	public void sendNotificationShouldReturnStatusOk() throws MalformedURLException, URISyntaxException {

		URIBuilder uriBuilder = new URIBuilder(NOTIFICATION_SERVICE_URL);
		uriBuilder.addParameter(NotificationServiceParameters.EMAIL.toString().toLowerCase(), EMAIL_FOR_TEST);
		uriBuilder.addParameter(NotificationServiceParameters.LAST_NAME.toString().toLowerCase(), LAST_NAME_FOR_TEST);
		uriBuilder.addParameter(NotificationServiceParameters.FIRST_NAME.toString().toLowerCase(), FIRST_NAME_FOR_TEST);
		uriBuilder.addParameter(NotificationServiceParameters.TYPE.toString().toLowerCase(), "ACCOUNT_CREATED");

		URI uriString = uriBuilder.build();

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<String> entity = new HttpEntity<String>(null, headers);

		Mockito.when(restTemplate.exchange(uriString, HttpMethod.POST, entity, String.class))
				.thenReturn(new ResponseEntity<String>(HttpStatus.CREATED));

		ReflectionTestUtils.setField(accountCreation, "notificationServiceUrl", NOTIFICATION_SERVICE_URL);

		HttpStatus result = accountCreation.sendRegisterEmail(EMAIL_FOR_TEST, LAST_NAME_FOR_TEST, FIRST_NAME_FOR_TEST);

		assertEquals(HttpStatus.CREATED, result);
	}

}
