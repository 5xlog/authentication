package com.pentastagiu.response;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.pentastagiu.enums.Status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ResponseStatusTest {

	@Test
	public void shouldReturnStatusOk() {

		assertEquals(ResponseStatus.createResponseStatusesList(Collections.emptyList()).get(0).getStatus(), Status.OK);
	}

	@Test
	public void shouldReturnStatusError() {

		assertEquals(ResponseStatus.createResponseStatusesList(Arrays.asList("any")).get(0).getStatus(), Status.ERROR);
	}

}
