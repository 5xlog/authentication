package com.pentastagiu.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.pentastagiu.dto.AccountDto;
import com.pentastagiu.enums.RegisterField;
import com.pentastagiu.enums.Status;
import com.pentastagiu.models.Account;
import com.pentastagiu.response.Response;
import com.pentastagiu.response.ResponseStatus;
import com.pentastagiu.service.RegisterService;
import com.pentastagiu.utils.AccountValidationConstants;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AuthenticationControllerTest {

	private MockMvc mockMvc;

	@InjectMocks
	AuthenticationController authenticationController;

	@Mock
	RegisterService registerService;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(authenticationController).build();
	}

	@Test
	public void shouldReturnStatusOk() throws Exception {
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		AccountDto accountDto = new AccountDto("Username", "Password123@", "Lastname", "Firstname", "emailp@ceva.com",
				"0712345678");

		when(registerService.register(Mockito.any())).thenReturn(new ArrayList<String>());

		this.mockMvc.perform(post("/register").contentType(MediaType.APPLICATION_JSON)
				.content(ow.writeValueAsString(accountDto))).andExpect(status().isOk());
	}

	@Test
	public void shouldReturnStatusBadRequest() throws Exception {
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		AccountDto accountDto = new AccountDto("1234", "Password", "123", "456", "email", "12345");

		List<ResponseStatus> responseStatuses = new ArrayList<>();
		List<String> toMock = new ArrayList<>();
		Response<Account> response = new Response<>();

		response.setData(null);

		for (RegisterField registerField : RegisterField.values()) {
			ResponseStatus responseStatus = new ResponseStatus();
			responseStatus.setStatus(Status.ERROR);
			responseStatus.setMessage(registerField.toString().toLowerCase().replace('_', ' ')
					+ AccountValidationConstants.ERROR_VALIDATION_MESSAGE);

			toMock.add(registerField.toString().toLowerCase().replace('_', ' ')
					+ AccountValidationConstants.ERROR_VALIDATION_MESSAGE);

			responseStatuses.add(responseStatus);
		}

		when(registerService.register(Mockito.any())).thenReturn(toMock);

		response.setResponseStatuses(responseStatuses);

		MvcResult result = this.mockMvc.perform(post("/register").contentType(MediaType.APPLICATION_JSON)
				.content(ow.writeValueAsString(accountDto))).andExpect(status().isBadRequest()).andReturn();

		JSONAssert.assertEquals(ow.writeValueAsString(response), result.getResponse().getContentAsString(), true);
	}

}
