package com.pentastagiu.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.pentastagiu.dto.ProductDTO;
import com.pentastagiu.response.Response;
import com.pentastagiu.service.ProductService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductControllerTest {

	private MockMvc mockMvc;

	@Mock
	ProductService productService;

	@InjectMocks
	ProductController productController;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(productController).build();
	}

	@SuppressWarnings("rawtypes")
	@Test
	public void testEditProductShouldReturnAccepted() throws JsonProcessingException, Exception {
		ProductDTO productDTO = new ProductDTO();
		ResponseEntity<Response> response = new ResponseEntity<Response>(HttpStatus.ACCEPTED);
		when(productService.editProduct(Mockito.any(), Mockito.any())).thenReturn(response);
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		mockMvc.perform(put("/product/{id}", 1).contentType(MediaType.APPLICATION_JSON)
				.content(ow.writeValueAsString(productDTO))).andExpect(status().isAccepted());
	}

	@SuppressWarnings("rawtypes")
	@Test
	public void testEditProductShouldReturnNotFound() throws Exception {
		ResponseEntity<Response> response = new ResponseEntity<Response>(HttpStatus.NOT_FOUND);
		when(productService.editProduct(Mockito.any(), Mockito.any())).thenReturn(response);
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		mockMvc.perform(put("/product/{id}", 1).contentType(MediaType.APPLICATION_JSON)
				.content(ow.writeValueAsString(response))).andExpect(status().isNotFound());
	}
}
