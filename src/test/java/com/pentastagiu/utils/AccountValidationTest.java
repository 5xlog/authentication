package com.pentastagiu.utils;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.pentastagiu.enums.Role;
import com.pentastagiu.models.Account;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AccountValidationTest {

	@Test
	public void shouldReturnEmptyList() {
		Account account = new Account("Username", "Password123@", Role.USER,
				"Lastname", "Firstname", "email@email.com", "0712345678");

		List<String> responsesStatus = AccountValidation.verifyAccount(account);

		assertThat(responsesStatus.isEmpty(), is(true));
	}

	@Test
	public void shouldReturnErrorsList() {
		Account account = new Account("123", "Password", Role.USER, "123",
				"123", "email", "12345678");
		List<String> expectedResponse = new ArrayList<>();

		expectedResponse.add(AccountValidationConstants.USERNAME_ERROR_MESSAGE);
		expectedResponse.add(AccountValidationConstants.PASSWORD_ERROR_MESSAGE);
		expectedResponse.add(AccountValidationConstants.EMAIL_ERROR_MESSAGE);
		expectedResponse.add(AccountValidationConstants.FIRST_NAME_ERROR_MESSAGE);
		expectedResponse.add(AccountValidationConstants.LAST_NAME_ERROR_MESSAGE);
		expectedResponse.add(AccountValidationConstants.PHONE_NUMBER_ERROR_MESSAGE);

		List<String> responsesStatus = AccountValidation.verifyAccount(account);

		assertThat(responsesStatus.isEmpty(), is(false));
		assertEquals(expectedResponse, responsesStatus);
	}
}
