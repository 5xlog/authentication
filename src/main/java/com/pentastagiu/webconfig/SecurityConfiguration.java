package com.pentastagiu.webconfig;

import javax.annotation.PostConstruct;
import javax.servlet.Filter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.context.WebApplicationContext;

import com.pentastagiu.security.AuthenticationFailureHandlerImpl;
import com.pentastagiu.security.AuthenticationFilter;
import com.pentastagiu.security.AuthenticationSuccessHandlerImpl;
import com.pentastagiu.security.CustomAuthenticationEntryPoint;
import com.pentastagiu.security.CustomUserDetailsService;
import com.pentastagiu.security.LogoutFilterImpl;
import com.pentastagiu.security.LogoutHandlerImpl;

/**
 * Provides the configuration for the web services
 * 
 * @author Constantin Listar
 *
 */
@EnableWebSecurity
@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	private WebApplicationContext applicationContext;
	private CustomUserDetailsService userDetailsService;
	@Autowired
	private AuthenticationSuccessHandlerImpl successHandler;
	@Autowired
	private AuthenticationFailureHandlerImpl failueHandler;
	@Autowired
	private LogoutHandlerImpl logoutHandler;
	@Autowired
	CustomAuthenticationEntryPoint alwaysSendUnauthorized401AuthenticationEntryPoint;
	
	@PostConstruct
	public void completeSetup() {
		userDetailsService = applicationContext.getBean(CustomUserDetailsService.class);
	}

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {

		httpSecurity.csrf().disable().exceptionHandling()
				.authenticationEntryPoint(alwaysSendUnauthorized401AuthenticationEntryPoint).and().authorizeRequests()
				.antMatchers("/product/*").authenticated().antMatchers("/changepass").authenticated()
				.antMatchers("/register").permitAll().antMatchers("/login").permitAll().and()
				.addFilter(getLogoutFilter()).addFilter(getAuthenticationFilter()).formLogin().loginPage("/login")
				.permitAll().successHandler(successHandler).failureHandler(failueHandler).and().logout()
				.logoutRequestMatcher(new AntPathRequestMatcher("/logout")).invalidateHttpSession(true)

				.defaultLogoutSuccessHandlerFor(new HttpStatusReturningLogoutSuccessHandler(HttpStatus.FORBIDDEN),
						new AntPathRequestMatcher("/logout"))
				.logoutSuccessHandler(new HttpStatusReturningLogoutSuccessHandler(HttpStatus.ACCEPTED))
				.addLogoutHandler(logoutHandler);
	}

	public Filter getLogoutFilter() throws Exception {
		LogoutFilter logoutFilter = new LogoutFilterImpl("", logoutHandler);
		return logoutFilter;
	}

	public Filter getAuthenticationFilter() throws Exception {
		UsernamePasswordAuthenticationFilter authenticationFilter = new AuthenticationFilter();
		authenticationFilter.setAuthenticationManager(this.authenticationManagerBean());
		authenticationFilter.setAuthenticationSuccessHandler(successHandler);
		authenticationFilter.setAuthenticationFailureHandler(failueHandler);
		return authenticationFilter;
	}

	@Override
	protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(encoder()).and()
				.authenticationProvider(authenticationProvider());
	}

	@Bean
	public DaoAuthenticationProvider authenticationProvider() {
		final DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		authProvider.setUserDetailsService(userDetailsService);
		authProvider.setPasswordEncoder(encoder());
		return authProvider;
	}

	@Bean
	public PasswordEncoder encoder() {
		return new BCryptPasswordEncoder();
	}

}
