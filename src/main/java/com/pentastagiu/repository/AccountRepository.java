package com.pentastagiu.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.pentastagiu.models.Account;

/**
 * Operations with the database
 * 
 * @author Constantin Listar
 *
 */
@Repository
public interface AccountRepository extends CrudRepository<Account, Long> {

	boolean existsByUsername(String username);

	boolean existsByEmail(String email);

	Optional<Account> findOneByUsernameOrEmail(String username, String email);

	Optional<Account> findOneByUsername(String username);
}
