package com.pentastagiu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.pentastagiu.dto.ProductDTO;
import com.pentastagiu.response.Response;
import com.pentastagiu.service.ProductService;

/**
 * Controller class exposing endpoints that work with Product project
 * 
 * @author Vacariuc Bogdan
 *
 */
@RestController
public class ProductController {

	@Autowired
	ProductService productService;

	/**
	 * Calls editProduct from ProductService that will do the operations needed.
	 * 
	 * @param id         id of the product that has to be changed
	 * @param productDTO object containing informations that will be changed
	 * @return return a ResponseEntity of Response that can be NOT_FOUND, if the
	 *         product could not be found or ACCEPTED if the product was found
	 */
	@SuppressWarnings("rawtypes")
	@PutMapping(value = "/product/{id}", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Response> editProduct(@PathVariable(value = "id") Long id,
			@RequestBody ProductDTO productDTO) {

		return productService.editProduct(id, productDTO);
	}

}
