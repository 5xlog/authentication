package com.pentastagiu.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.pentastagiu.dto.AccountDto;
import com.pentastagiu.enums.Status;
import com.pentastagiu.response.Response;
import com.pentastagiu.response.ResponseStatus;
import com.pentastagiu.service.AccountService;
import com.pentastagiu.service.RegisterService;
import com.pentastagiu.utils.AccountValidationConstants;

/**
 * Controller for authentication
 * 
 * @author Constantin Listar
 *
 */
@EnableWebMvc
@RestController
public class AuthenticationController {

	@Autowired
	RegisterService registerService;

	@Autowired
	AccountService accountService;

	/**
	 * 
	 * Consumes the body from "/register" and produces a JSON containing the
	 * response based on the data returned from the service
	 * 
	 * @param gets an account object from the body
	 * 
	 * @author Constantin Listar
	 *
	 */
	@PostMapping(path = "/register", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Response<String>> createAccount(@RequestBody AccountDto accountDto) {

		List<String> registerServiceResponse = registerService.register(accountDto);

		Response<String> response = new Response<>();
		response.setResponseStatuses(ResponseStatus.createResponseStatusesList(registerServiceResponse));

		if (response.getResponseStatuses().get(0).getMessage()
				.equals(AccountValidationConstants.ACCOUNT_CREATION_MESSAGE)) {
			return ResponseEntity.ok(response);
		} else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
	}

	/**
	 * Gets the new password from the requested body and response with OK if the
	 * modify had been changed and BAD_REQUEST if the modify could not be made
	 * 
	 * @param principal Logged user
	 * @param password  new password
	 * @return
	 */
	@PutMapping(path = "/changepass", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Response<String>> changePassword(Principal principal,
			@RequestBody(required = true) String password) {
		return ResponseEntity.ok(accountService.changePassword(principal.getName(), password));
	}

	public List<ResponseStatus> createResponseStatusesList(List<String> registerServiceResponse) {
		List<ResponseStatus> responseStatuses = new ArrayList<>();

		if (registerServiceResponse.isEmpty()) {
			ResponseStatus responseStatus = new ResponseStatus();
			responseStatus.setStatus(Status.OK);
			responseStatus.setMessage(AccountValidationConstants.ACCOUNT_CREATION_MESSAGE);
			responseStatuses.add(responseStatus);
		} else {
			for (String ErrorMessage : registerServiceResponse) {
				ResponseStatus responseStatus = new ResponseStatus();
				responseStatus.setStatus(Status.ERROR);
				responseStatus.setMessage(ErrorMessage);
				responseStatuses.add(responseStatus);
			}
		}

		return responseStatuses;
	}
}
