package com.pentastagiu.response;

import java.util.ArrayList;
import java.util.List;

import com.pentastagiu.enums.Status;
import com.pentastagiu.utils.AccountValidationConstants;

/**
 * Represents the model for the response status
 * 
 * @author Constantin Listar
 *
 */
public class ResponseStatus {

	private String message;
	private Status status;
	
	public ResponseStatus(String message, Status status) {
		this.message = message;
		this.status = status;
	}

	public ResponseStatus() {
		// TODO Auto-generated constructor stub
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	/**
	 * Creates a list of responseStatus
	 * 
	 * @param registerServiceResponse the response from the service
	 * @return a list of responseStatus for the Response object
	 */
	public static List<ResponseStatus> createResponseStatusesList(List<String> registerServiceResponse) {
		List<ResponseStatus> responseStatuses = new ArrayList<>();

		if (registerServiceResponse.isEmpty()) {
			ResponseStatus responseStatus = new ResponseStatus();
			responseStatus.setStatus(Status.OK);
			responseStatus.setMessage(AccountValidationConstants.ACCOUNT_CREATION_MESSAGE);
			responseStatuses.add(responseStatus);
		} else {
			for (String ErrorMessage : registerServiceResponse) {
				ResponseStatus responseStatus = new ResponseStatus();
				responseStatus.setStatus(Status.ERROR);
				responseStatus.setMessage(ErrorMessage);
				responseStatuses.add(responseStatus);
			}
		}

		return responseStatuses;
	}

}
