package com.pentastagiu.response;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents the model for the response
 * 
 * @author Constantin Listar
 *
 */
public class Response<T extends Object> {

	private T data;
	private List<ResponseStatus> responseStatuses;

	public Response() {
		responseStatuses = new ArrayList<ResponseStatus>();
	}
	
	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public List<ResponseStatus> getResponseStatuses() {
		return responseStatuses;
	}

	public void setResponseStatuses(List<ResponseStatus> responseStatuses) {
		this.responseStatuses = responseStatuses;
	}
	
	public void addResponseStatus(ResponseStatus responseStatus) {
		responseStatuses.add(responseStatus);
	}
}
