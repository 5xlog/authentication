package com.pentastagiu.handler;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.RestClientException;

import com.pentastagiu.enums.Status;
import com.pentastagiu.response.Response;
import com.pentastagiu.response.ResponseStatus;

@ControllerAdvice
public class RestControllerAdvice {

	Logger LOGGER = LoggerFactory.getLogger(RestControllerAdvice.class);

	@ExceptionHandler(Exception.class)
	public ResponseEntity<Response<String>> handleAllExceptions(HttpServletRequest request, Exception ex) {
		ResponseStatus responseStatus = new ResponseStatus("Server is currently down. Try again later!", Status.ERROR);
		Response<String> response = new Response<String>();
		response.setData(null);
		response.addResponseStatus(responseStatus);
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
	}

	@ExceptionHandler(RestClientException.class)
	public ResponseEntity<Response<String>> handleRestClientException(HttpServletRequest request,
			RestClientException ex) {
		ResponseStatus responseStatus = new ResponseStatus("Request could not be made.", Status.ERROR);
		Response<String> response = new Response<String>();
		response.setData(null);
		response.addResponseStatus(responseStatus);
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
	}

	@ExceptionHandler(SecurityException.class)
	public ResponseEntity<Response<String>> handlePasswordException(HttpServletRequest request, SecurityException ex) {
		ResponseStatus responseStatus = new ResponseStatus(ex.getMessage(), Status.ERROR);
		Response<String> response = new Response<String>();
		response.setData(null);
		response.addResponseStatus(responseStatus);
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
	}
}
