package com.pentastagiu.utils;

import java.util.ArrayList;
import java.util.List;

import com.pentastagiu.enums.RegisterField;
import com.pentastagiu.models.Account;

/**
 * Verifies if the account details are valid
 * 
 * @author Constantin Listar
 *
 */
public final class AccountValidation {

	private AccountValidation() {
	}

	public static List<String> verifyAccount(Account account) {

		List<String> responsesStatus = new ArrayList<>();

		if (!verifyUsername(account.getUsername())) {
			responsesStatus.add(AccountValidationConstants.USERNAME_ERROR_MESSAGE);
		}

		if (!verifyPassword(account.getPassword())) {
			responsesStatus.add(AccountValidationConstants.PASSWORD_ERROR_MESSAGE);
		}

		if (!verifyEmail(account.getEmail())) {
			responsesStatus.add(AccountValidationConstants.EMAIL_ERROR_MESSAGE);
		}

		if (!verifyFirstName(account.getFirstName())) {
			responsesStatus.add(AccountValidationConstants.FIRST_NAME_ERROR_MESSAGE);
		}

		if (!verifyLastName(account.getLastName())) {
			responsesStatus.add(AccountValidationConstants.LAST_NAME_ERROR_MESSAGE);
		}

		if (!verifyPhoneNumber(account.getPhone())) {
			responsesStatus.add(AccountValidationConstants.PHONE_NUMBER_ERROR_MESSAGE);
		}

		return responsesStatus;
	}

	private static boolean verifyUsername(String username) {
		return (username != null && username.matches(AccountValidationConstants.USERNAME_PATTERN));
	}

	public static boolean verifyPassword(String password) {
		return (password != null && password.matches(AccountValidationConstants.PASSWORD_PATTERN));
	}

	private static boolean verifyEmail(String email) {
		return (email != null && email.matches(AccountValidationConstants.EMAIL_PATTERN));
	}

	private static boolean verifyFirstName(String firstName) {
		return (firstName != null && firstName.matches(AccountValidationConstants.FIRST_NAME_PATTERN));
	}

	private static boolean verifyLastName(String lastName) {
		return (lastName != null && lastName.matches(AccountValidationConstants.LAST_NAME_PATTERN));
	}

	private static boolean verifyPhoneNumber(String phoneNumber) {
		return (phoneNumber != null && phoneNumber.matches(AccountValidationConstants.PHONE_NUMBER_PATTERN));
	}

	public static String createAvailabilityErrorMessage(RegisterField registerField) {
		return registerField.toString().toLowerCase().replace('_', ' ')
				+ AccountValidationConstants.ERROR_AVAILABILITY_MESSAGE;
	}

}
