package com.pentastagiu.utils;

import com.pentastagiu.dto.AccountDto;
import com.pentastagiu.enums.Role;
import com.pentastagiu.models.Account;

/**
 * DTO class that is responsible with the account conversion
 * 
 * @author Constantin Listar
 *
 */
public final class AccountDtoConverter {

	private AccountDtoConverter() {
	}

	/**
	 * Adds the role(USER), the modified date and the creation date to the new
	 * account
	 * 
	 * @param accountDto the raw account
	 * @return the final account
	 */
	public static Account fromAccountDtoToAccount(AccountDto accountDto) {
		Account account = new Account(accountDto.getUsername(), accountDto.getPassword(), Role.USER,
			 accountDto.getLastName(), accountDto.getFirstName(),
				accountDto.getEmail(), accountDto.getPhone());

		return account;
	}
}