package com.pentastagiu.utils;

/**
 * Contains constants needed for account validation
 * 
 * @author Constantin Listar
 *
 */
public final class AccountValidationConstants {
	public static final String USERNAME_PATTERN = "[A-Za-z0-9_]+.{5,20}";
	public static final String PASSWORD_PATTERN = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{5,20}";
	public static final String EMAIL_PATTERN = "^[A-Za-z0-9+_.-]+@(.+)$";
	public static final String FIRST_NAME_PATTERN = "[A-Z][a-z]*";
	public static final String LAST_NAME_PATTERN = "[A-Z][a-z]*";
	public static final String PHONE_NUMBER_PATTERN = "07\\d{8}";

	public static final String ERROR_VALIDATION_MESSAGE = " is wrong";
	public static final String SUCCESS_VALIDATION_MESSAGE = "Account details are valid";
	public static final String ERROR_AVAILABILITY_MESSAGE = " is already used by another account";
	public static final String NOT_AVAILABLE_MESSAGE = " is not available";
	public static final String ACCOUNT_CREATION_MESSAGE = "The account was created successfully";
	public static final String REGISTER_ERROR_MESSAGE = "You can't create an account right now.";

	public static final String DATABASE_ERROR_MESSAGE = "The account couldn't be saved in the database";
	public static final String USERNAME_ERROR_MESSAGE = "The username should contain 5 to 20 characters that can be letters,"
			+ " digits or the special character \"_\".";
	public static final String PASSWORD_ERROR_MESSAGE = "The password should contain 5 to 20 characters; it needs to contain at least one"
			+ " digit, at least one lower alpha char and one upper alpha char and at least one"
			+ " char within a set of special chars (@#%$^); also no whitespace is allowed in the entire string.";
	public static final String EMAIL_ERROR_MESSAGE = "The email can contain only letters, digits and one of the special characters(@).";
	public static final String FIRST_NAME_ERROR_MESSAGE = "The first name should start with an upper alpha char and can contain only letters.";
	public static final String LAST_NAME_ERROR_MESSAGE = "The last name should start with an upper alpha char and can contain only letters.";
	public static final String PHONE_NUMBER_ERROR_MESSAGE = "The phone number should have the \"07\" prefix followed by another 8 digits.";
	public static final String PASSWORD_NOT_GOOD = "Password does not meet the required pattern.";
	public static final String PASSWORD_CHANGED = "Password has been changed successfully";
	public static final String NOTIFICATION_ERROR_MESSAGE = "Notification could not be processed";
	public static final String PASSWORD_CHANGED_ERROR_MESSAGE = "Servers are unable to execute this operation. Try again later!";
	
	private AccountValidationConstants() {
	}
}
