package com.pentastagiu.security;

/**
 * Object used to map login details from the body of the /login requests
 * 
 * @author Vacariuc Bogdan
 *
 */
public class LoginRequest {

	private String username;
	private String password;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}