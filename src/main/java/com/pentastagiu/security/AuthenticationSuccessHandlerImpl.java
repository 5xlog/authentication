package com.pentastagiu.security;

import java.io.IOException;
import java.time.LocalDateTime;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.pentastagiu.models.Account;
import com.pentastagiu.repository.AccountRepository;

/**
 * Handler used for authentication with success
 * 
 * @author Vacariuc Bogdan
 *
 */
@Component
public class AuthenticationSuccessHandlerImpl implements AuthenticationSuccessHandler {

	@Autowired
	private AccountRepository accountRepository;

	/**
	 * When the user authenticates with success the column last_login from database
	 * will be updated
	 */
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {

		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Account account = ((AccountPrincipal) principal).getAppUser();
		account.setLastLogin(LocalDateTime.now());
		accountRepository.save(account);
		response.setStatus(HttpStatus.ACCEPTED.value());
	}
}