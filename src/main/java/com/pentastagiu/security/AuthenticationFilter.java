package com.pentastagiu.security;

import java.io.BufferedReader;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Used to get the username and password from body, not as request param
 * 
 * @author Vacariuc Bogdan
 *
 */
public class AuthenticationFilter extends UsernamePasswordAuthenticationFilter {
	private final Logger LOGGER = LoggerFactory.getLogger(AuthenticationFilter.class);

	private String jsonUsername;
	private String jsonPassword;

	@Override
	@Autowired
	public void setAuthenticationManager(AuthenticationManager authenticationManager) {
		super.setAuthenticationManager(authenticationManager);
	}

	@Override
	protected String obtainPassword(HttpServletRequest request) {
		String password = null;

		if ("application/json".equals(request.getHeader("Content-Type"))) {
			password = this.jsonPassword;
		} else {
			password = super.obtainPassword(request);
		}

		return password;
	}

	@Override
	protected String obtainUsername(HttpServletRequest request) {
		String username = null;

		if ("application/json".equals(request.getHeader("Content-Type"))) {
			username = this.jsonUsername;
		} else {
			username = super.obtainUsername(request);
		}

		return username;
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {
		if ("application/json".equals(request.getHeader("Content-Type"))) {
			try {
				/*
				 * HttpServletRequest can be read only once
				 */
				StringBuffer sb = new StringBuffer();
				String line = null;

				BufferedReader reader = request.getReader();
				while ((line = reader.readLine()) != null) {
					sb.append(line);
				}

				// json transformation
				ObjectMapper mapper = new ObjectMapper();
				LoginRequest loginRequest = mapper.readValue(sb.toString(), LoginRequest.class);

				this.jsonUsername = loginRequest.getUsername();
				this.jsonPassword = loginRequest.getPassword();
			} catch (Exception e) {
				LOGGER.debug(
						"Error in authentication filter. Something went wrong with mapping the body to the LoginRequest class");
			}
		}

		return super.attemptAuthentication(request, response);
	}

}
