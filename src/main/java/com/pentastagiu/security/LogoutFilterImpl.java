package com.pentastagiu.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.LogoutFilter;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

/**
 * Logout filter used to respond with FORBIDDEN status if the user is not logged
 * in
 * 
 * @author Vacariuc Bogdan
 *
 */
public class LogoutFilterImpl extends LogoutFilter {
	public LogoutFilterImpl(LogoutSuccessHandler logoutSuccessHandler, LogoutHandler... handlers) {
		super(logoutSuccessHandler, handlers);
	}

	public LogoutFilterImpl(String logoutSuccessUrl, LogoutHandler... handlers) {
		super(logoutSuccessUrl, handlers);
	}

	@Override
	protected boolean requiresLogout(HttpServletRequest request, HttpServletResponse response) {
		boolean result = super.requiresLogout(request, response);
		return result;
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;

		if (requiresLogout(request, response)) {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			request.logout();

			if (logger.isDebugEnabled()) {
				logger.debug("Logging out user '" + auth + "' and transferring to logout destination");
			}

			try {
				if (auth.isAuthenticated()) {
					response.setStatus(HttpStatus.ACCEPTED.value());
					auth.setAuthenticated(false);
					Cookie cookie = new Cookie("JSESSIONID", null);
					cookie.setMaxAge(0);
					request.logout();
					return;
				} else {
					response.setStatus(HttpStatus.FORBIDDEN.value());
					return;
				}
			} catch (Exception e) {
				response.setStatus(HttpStatus.FORBIDDEN.value());
				return;
			}
		}
		chain.doFilter(request, response);
	}
}
