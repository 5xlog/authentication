package com.pentastagiu.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

/**
 * Handler for a failed authentication
 * 
 * @author Vacariuc Bogdan
 *
 */
@Component
public class AuthenticationFailureHandlerImpl implements AuthenticationFailureHandler {

	/**
	 * It sets the http status on BAD_REQUEST
	 */
	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException {
		response.setStatus(HttpStatus.FORBIDDEN.value());

	}

}
