package com.pentastagiu.security;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.pentastagiu.models.Account;
import com.pentastagiu.repository.AccountRepository;

/**
 * Implementation of UserDetailsService
 * 
 * It is used throughout the framework as a user DAO and is the strategy used by
 * the DaoAuthenticationProvider.
 * 
 * @author Vacariuc Bogdan
 *
 */
@Service
public class CustomUserDetailsService implements UserDetailsService {

	private AccountRepository accountRepository;

	public CustomUserDetailsService() {
		super();
	}

	@Autowired
	public CustomUserDetailsService(AccountRepository accountRepository) {
		this.accountRepository = accountRepository;
	}

	/**
	 * Locates the user based on the username. In the actual implementation, the
	 * search may possibly be case sensitive, or case insensitive depending on how
	 * the implementation instance is configured. In this case, the
	 * UserDetailsobject that comes back may have a username that is of a different
	 * case than what was actually requested.
	 */
	@Override
	public UserDetails loadUserByUsername(final String username) {
		final Optional<Account> appUser = accountRepository.findOneByUsernameOrEmail(username, username);
		if (!appUser.isPresent()) {
			throw new UsernameNotFoundException(username);
		}
		return new AccountPrincipal(appUser.get());
	}

}