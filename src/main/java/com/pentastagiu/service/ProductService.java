package com.pentastagiu.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.pentastagiu.dto.ProductDTO;
import com.pentastagiu.response.Response;

/**
 * Service class for operations with products from Product project
 * 
 * @author Vacariuc Bogdan
 *
 */
@Service
public class ProductService {

	@Value("${url.product.edit}")
	String uri;

	RestTemplate restTemplate;

	@Autowired
	public ProductService(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	/**
	 * Consumes a RESTful service and returns the response. If the status is 4xx,
	 * the body will not be available through getBody() method and a
	 * HttpClientErrorException is thrown, so the body had to be taken within the
	 * message of the exception thrown by the exchange() method.
	 * 
	 * @param id         id of the product that has to be changed
	 * @param productDTO mapped informations that will be changed
	 * @return return a ResponseEntity of Response that can be NOT_FOUND, if the
	 *         product could not be found or ACCEPTED if the product was found
	 * @see Gson
	 */
	@SuppressWarnings("rawtypes")
	public ResponseEntity<Response> editProduct(Long id, ProductDTO productDTO) {
		HttpEntity<ProductDTO> request = new HttpEntity<ProductDTO>(productDTO);
		try {
			return restTemplate.exchange(uri, HttpMethod.PUT, request, Response.class, id);
		} catch (final HttpClientErrorException e) {
			Gson gson = new Gson();
			Response p = gson.fromJson(e.getResponseBodyAsString(), Response.class);
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(p);
		}
	}
}
