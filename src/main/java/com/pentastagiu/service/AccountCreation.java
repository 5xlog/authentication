package com.pentastagiu.service;

import java.net.MalformedURLException;

import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.client.utils.URIBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.pentastagiu.enums.NotificationServiceParameters;

/**
 * Class responsible with calls to other services upon account creation
 * 
 * @author Constantin Listar
 *
 */
@Service
public class AccountCreation {

	private static final String NOTIFICATION_TYPE = "ACCOUNT_CREATED";

	@Value("${url.cart.add}")
	private String cartServiceUrl;

	@Value("${url.notification.add}")
	private String notificationServiceUrl;

	@Autowired
	private RestTemplate restTemplate;

	/**
	 * Calls the cart service in order to create a cart for the new account
	 * specified by his accountId
	 * 
	 * @param accountId the id of the account that needs to have a new cart
	 * @return the status from the cart service
	 */
	public HttpStatus createCartUponAccountCreation(Long accountId) {

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<String> entity = new HttpEntity<String>(null, headers);
		ResponseEntity<String> result = restTemplate.exchange(cartServiceUrl + accountId, HttpMethod.POST, entity,
				String.class);

		return result.getStatusCode();
	}

	/**
	 * Sends an email to the new created account
	 * 
	 * @param email
	 * @param lastName
	 * @param firstName
	 * @param type
	 * @return
	 * @throws URISyntaxException
	 * @throws MalformedURLException
	 */
	public HttpStatus sendRegisterEmail(String email, String lastName, String firstName)
			throws URISyntaxException, MalformedURLException {

		URIBuilder uriBuilder = new URIBuilder(notificationServiceUrl);
		uriBuilder.addParameter(NotificationServiceParameters.EMAIL.toString().toLowerCase(), email);
		uriBuilder.addParameter(NotificationServiceParameters.LAST_NAME.toString().toLowerCase(), lastName);
		uriBuilder.addParameter(NotificationServiceParameters.FIRST_NAME.toString().toLowerCase(), firstName);
		uriBuilder.addParameter(NotificationServiceParameters.TYPE.toString().toLowerCase(), NOTIFICATION_TYPE);

		URI uriString = uriBuilder.build();

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<String> entity = new HttpEntity<String>(null, headers);

		ResponseEntity<String> result = restTemplate.exchange(uriString, HttpMethod.POST, entity, String.class);

		return result.getStatusCode();
	}

}
