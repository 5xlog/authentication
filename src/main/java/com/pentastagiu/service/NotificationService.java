package com.pentastagiu.service;

import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Service class used for making requests to the notification project
 * 
 * @author Vacariuc Bogdan
 *
 */
@Service
public class NotificationService {

	Logger LOGGER = LoggerFactory.getLogger(NotificationService.class);

	@Value("${url.notification.add}")
	String uri;

	@Autowired
	RestTemplate restTemplate;

	/**
	 * Creates the URL and executes the http method to the given URI template to use
	 * the createNotification services in the Notification project
	 * 
	 * @param email
	 * @param lastName
	 * @param firstName
	 * @param type
	 */
	public void sendNotification(String email, String lastName, String firstName, String type) {
		URIBuilder uriBuilder;
		try {
			uriBuilder = new URIBuilder(uri);

			uriBuilder.addParameter("email", email);
			uriBuilder.addParameter("last_name", lastName);
			uriBuilder.addParameter("first_name", firstName);
			uriBuilder.addParameter("type", type);

			URI uriString = uriBuilder.build();

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);

			HttpEntity<String> entity = new HttpEntity<String>(null, headers);

			restTemplate.exchange(uriString, HttpMethod.POST, entity, String.class);

		} catch (URISyntaxException e) {
			LOGGER.debug("URI could not be built!");
		}
	}
}
