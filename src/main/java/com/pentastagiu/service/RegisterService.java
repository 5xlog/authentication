package com.pentastagiu.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import com.pentastagiu.dto.AccountDto;
import com.pentastagiu.enums.RegisterField;
import com.pentastagiu.models.Account;
import com.pentastagiu.repository.AccountRepository;
import com.pentastagiu.utils.AccountDtoConverter;
import com.pentastagiu.utils.AccountValidation;
import com.pentastagiu.utils.AccountValidationConstants;

/**
 * Responsible with the account registration
 * 
 * @author Constantin Listar
 *
 */
@Service
public class RegisterService {

	@Autowired
	private AccountRepository accountRepository;

	@Autowired
	private AccountService accountService;

	@Autowired
	private AccountCreation accountCreation;

	/**
	 * If the account details are valid and available saves the account in the
	 * database
	 * 
	 * @param the Account(object) that needs to be inserted in the database
	 * @return a list of messages, depending on the account details
	 * 
	 * @author Constantin Listar
	 *
	 */
	public List<String> register(AccountDto accountDto) {

		Account account = AccountDtoConverter.fromAccountDtoToAccount(accountDto);

		List<String> validationMessages = AccountValidation.verifyAccount(account);
		if (validationMessages.isEmpty()) {

			if (accountService.verifyByEmailIfAccountExists(account.getEmail())) {
				validationMessages.add(RegisterField.EMAIL.toString().toLowerCase().replace('_', ' ')
						+ AccountValidationConstants.ERROR_AVAILABILITY_MESSAGE);
			}

			if (accountService.verifyByUsernameIfAccountExists(account.getUsername())) {
				validationMessages.add(RegisterField.USERNAME.toString().toLowerCase().replace('_', ' ')
						+ AccountValidationConstants.ERROR_AVAILABILITY_MESSAGE);
			}
		}

		if (validationMessages.isEmpty()) {
			try {
				account.setPassword(BCrypt.hashpw(account.getPassword(), BCrypt.gensalt()));

				Long accountId = accountRepository.save(account).getAccountId();

				if (!accountCreation.createCartUponAccountCreation(accountId).equals(HttpStatus.OK)) {
					accountRepository.deleteById(accountId);

					validationMessages.add(AccountValidationConstants.REGISTER_ERROR_MESSAGE);
				} else {
					accountCreation.sendRegisterEmail(account.getEmail(), account.getLastName(),
							account.getFirstName());
				}

			} catch (Exception e) {
				validationMessages.add(AccountValidationConstants.DATABASE_ERROR_MESSAGE);
			}
		}

		return validationMessages;
	}

}
