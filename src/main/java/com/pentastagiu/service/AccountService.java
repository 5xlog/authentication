package com.pentastagiu.service;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import com.pentastagiu.enums.Status;
import com.pentastagiu.models.Account;
import com.pentastagiu.repository.AccountRepository;
import com.pentastagiu.response.Response;
import com.pentastagiu.response.ResponseStatus;
import com.pentastagiu.utils.AccountValidationConstants;

/**
 * Service class for operations on accounts
 * 
 * @author Vacariuc Bogdan
 *
 */
@Service
public class AccountService {

	Logger LOGGER = LoggerFactory.getLogger(AccountService.class);

	AccountRepository accountRepository;
	NotificationService notificationService;

	@Autowired
	public AccountService(AccountRepository accountRepository, NotificationService notificationService) {
		this.accountRepository = accountRepository;
		this.notificationService = notificationService;
	}

	/**
	 * Verifies if there is an account with the specified username
	 * 
	 * @param username of the account
	 * @return true if it already exists, else false
	 */
	public boolean verifyByUsernameIfAccountExists(String username) {
		return accountRepository.existsByUsername(username);
	}

	/**
	 * Verifies if there is an account with the specified email
	 * 
	 * @param email of the account
	 * @return true if it already exists, else false
	 */
	public boolean verifyByEmailIfAccountExists(String email) {
		return accountRepository.existsByEmail(email);
	}

	/**
	 * Changes password for the specified username and sends notification
	 * 
	 * @param username username of the logged user
	 * @param password new password
	 * @return
	 */
	public Response<String> changePassword(String username, String password) {
		Response<String> response = new Response<String>();
		response.setData(null);
		Optional<Account> account = accountRepository.findOneByUsername(username);
		Account loggedAccount = account.get();

		if (password.matches(AccountValidationConstants.PASSWORD_PATTERN)) {
			loggedAccount.setPassword(BCrypt.hashpw(password, BCrypt.gensalt()));
			notificationService.sendNotification(loggedAccount.getEmail(), loggedAccount.getLastName(),
					loggedAccount.getFirstName(), "PASSWORD_RESET");
			accountRepository.save(loggedAccount);
			response.addResponseStatus(new ResponseStatus(AccountValidationConstants.PASSWORD_CHANGED, Status.OK));
			return response;
		} else {
			throw new SecurityException(AccountValidationConstants.PASSWORD_ERROR_MESSAGE);
		}
	}
}
