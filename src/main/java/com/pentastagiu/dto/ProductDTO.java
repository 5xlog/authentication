package com.pentastagiu.dto;

import java.math.BigDecimal;

/**
 * Data transfer object used as body in rest controller
 * 
 * @author Vacariuc Bogdan
 *
 */

public class ProductDTO {

	private String name;

	private String description;

	private BigDecimal price;

	private Integer stock;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}
}
