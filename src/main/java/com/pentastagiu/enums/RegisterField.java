package com.pentastagiu.enums;

/**
 * Contains all the fields required for register
 * 
 * @author Constantin Listar
 *
 */
public enum RegisterField {
	USERNAME, PASSWORD, EMAIL, FIRST_NAME, LAST_NAME, PHONE_NUMBER
}
