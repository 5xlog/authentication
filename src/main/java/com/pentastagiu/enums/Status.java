package com.pentastagiu.enums;

/**
 * Contains the final status of the service
 * 
 * @author Constantin Listar
 *
 */
public enum Status {
	ERROR, OK
}
