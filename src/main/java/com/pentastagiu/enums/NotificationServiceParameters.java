package com.pentastagiu.enums;

/**
 * Contains the parameters needed for calling the notification service
 * 
 * 
 * @author Constantin Listar
 *
 */
public enum NotificationServiceParameters {
	EMAIL, FIRST_NAME, LAST_NAME, TYPE
}
