package com.pentastagiu.enums;

/**
 * Contains all the possible roles for an account
 * 
 * @author Constantin Listar
 *
 */
public enum Role {
	USER, ADMIN, COURIER
}
